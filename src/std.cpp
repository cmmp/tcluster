#include <iostream>
#include <limits>
#include <Rcpp.h>

using namespace std;
using namespace Rcpp;

static double tol = 1E-6;

extern bool isequal(double x, double y);

//bool isequal(double x, double y) {
//  if(abs(x-y) > tol)
//    return(false);
//  return(true);
//}
 
void print_vector(NumericVector x) {
  //  for(NumericVector::iterator it = x.begin(); it != x.end(); it++)
    //    cout << *it << " ";
    //  cout << endl;
}

RcppExport SEXP weightedsd_cpp(SEXP xin) {
  NumericVector x(xin);
  NumericVector xsort(clone(x));

  if(x.size() == 1) {
    List ret;
    ret["weighted_sd"] = 0;
    ret["split_point"] = x[0];
    return(ret);
  }
  
  sort(xsort.begin(), xsort.end());
  
  List ret;  
  int N = xsort.size();
  double minsd = numeric_limits<double>::max();
  double bestSplitPoint = -1;

  for(int i = 1; i < N ; i++) {
    NumericVector xpart1(xsort.begin(), xsort.begin()+i);
    NumericVector xpart2(xsort.begin()+i, xsort.end());
    if(isequal(xpart1[xpart1.size()-1],xpart2[0])) // dont split where the endpoints are equal.
      continue;
    // cout << "xpart1: " << endl;
    // print_vector(xpart1);
    // cout << "xpart2: " << endl;
    // print_vector(xpart2);
    // cout << "-------------" << endl;
    double stdpart1 = 0, stdpart2 = 0;
    double *v;
    double wsd = -1; // weighted sd
    if(xpart1.size() > 1)
      stdpart1 = sd(xpart1);
    if(xpart2.size() > 1)
      stdpart2 = sd(xpart2);

    wsd = (i * stdpart1 + (N-i)*stdpart2) / N;
    if(wsd < minsd) {
      minsd = wsd;
      bestSplitPoint = (xpart1[xpart1.size()-1] + xpart2[0]) / 2.0; // pick the mid point
    }
  }
  ret["weighted_sd"] = minsd;
  ret["split_point"] = bestSplitPoint;
  
  return(ret);
}
