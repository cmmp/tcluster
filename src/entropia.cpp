#include <iostream>
#include <limits>
//#include <Rcpp.h>
#include <RcppArmadillo.h>

using namespace std;
using namespace Rcpp;
using namespace arma;

extern void print_vector(NumericVector x);

Environment graphics("package:graphics");
//Environment entropy("package:entropy");
Function hist = graphics["hist"];
//Function entropyf = entropy["entropy"];

static double tol = 1E-6;

bool isequal(double x, double y) {
  if(abs(x-y) > tol)
    return(false);
  return(true);
}

// double compute_entropy(NumericVector x) {
//   SEXP h = hist(x, _["plot"] = 0);
//   List reth(h);

//   NumericVector mycounts = reth["counts"];

//   SEXP entret = entropyf(mycounts, _["unit"] = "log2");

//   NumericVector ent(entret);

//   return(ent[0]);
// }

// double compute_entropy(NumericVector x) {
//   SEXP h = hist(x, _["plot"] = 0);
//   List reth(h);

//   NumericVector mycounts = reth["counts"];
  
//   double soma = sum(mycounts);
//   NumericVector probs(mycounts.size());
//   double ent = 0;

//   probs = mycounts / soma; // vectorized calculation using Rcpp sugar.
  
//   for(int i = 0; i < probs.size(); i++) {
//     if(probs[i] > 0)
//       ent = ent + probs[i] * log2(probs[i]);
//   }

//   return(-ent);
// }

RcppExport SEXP histogram_cpp(SEXP xin, SEXP _nbins) {
  NumericVector x(xin);
  colvec y(x.begin(), x.size(), false);
  int nbins = as<int>(_nbins);

  uvec h = arma::hist(y, nbins); // second argument is the number of bins
  return(wrap(h));
}

double compute_entropy(NumericVector x, int nbins) {
  //  cout << "fui chamado com nbins = " << nbins << endl;
  if (x.size() < 2) return(0);
  colvec y(x.begin(), x.size(), false);

  uvec h = arma::hist(y, nbins); // second argument is the number of bins

  NumericVector mycounts = wrap(h);

  double soma = sum(mycounts);
  NumericVector probs(mycounts.size());
  double ent = 0;

  probs = mycounts / soma; // vectorized calculation using Rcpp sugar.
  
  for(int i = 0; i < probs.size(); i++) {
    if(probs[i] > 0)
      ent = ent + probs[i] * log2(probs[i]);
  }

  return(-ent);
}

RcppExport SEXP computeentropy_cpp(SEXP xin, SEXP _nbins) {
  NumericVector x(xin);
  int nbins = as<int>(_nbins);
  return(wrap(compute_entropy(x, nbins)));
}

RcppExport SEXP weightedentropy_cpp(SEXP xin, SEXP _nbins) {
  NumericVector x(xin);
  int nbins = as<int>(_nbins);

  NumericVector xsort(clone(x));

  if(x.size() == 1) {
    List ret;
    ret["weighted_ent"] = 0;
    ret["split_point"] = x[0];
    return(ret);
  }
  
  sort(xsort.begin(), xsort.end());
  
  List ret;  
  int N = xsort.size();
  double minentr = numeric_limits<double>::max();
  double bestSplitPoint = -1;

  for(int i = 1; i < N ; i++) {
    NumericVector xpart1(xsort.begin(), xsort.begin()+i);
    NumericVector xpart2(xsort.begin()+i, xsort.end());
    if(isequal(xpart1[xpart1.size()-1],xpart2[0])) // dont split where the endpoints are equal.
      continue;

    double entpart1 = 0, entpart2 = 0;
    double *v;
    double went = -1; // weighted entropy
    if(xpart1.size() > 1)
      entpart1 = compute_entropy(xpart1, nbins);
    if(xpart2.size() > 1)
      entpart2 = compute_entropy(xpart2, nbins);

    went = (i * entpart1 + (N-i)*entpart2) / N;
    if(went < minentr) {
      minentr = went;
      bestSplitPoint = (xpart1[xpart1.size()-1] + xpart2[0]) / 2.0; // pick the mid point
    }
  }
  ret["weighted_ent"] = minentr;
  ret["split_point"] = bestSplitPoint;
  
  return(ret);
}
