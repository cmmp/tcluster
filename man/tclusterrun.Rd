\name{tclusterrun}
\alias{tclusterrun}
\title{Function to run the TCluster algorithm}
\usage{
  tclusterrun(dados = NULL, W_length = 200, NCOEFS = 20,
    periodicEval = FALSE, evalMetric = NULL,
    expectedLabels = NULL, verbose = TRUE,
    printAggSplit = FALSE, alpha = 0.3, beta = 0.3,
    lambda = 0.5, measures = c("FOU", "AMI", "HURST"))
}
\arguments{
  \item{dados}{a matrix with time series in the rows}

  \item{W_length}{size of window block}

  \item{NCOEFS}{number of coefficients for quantifiers}

  \item{periodicEval}{logical:should a periodic evaluation
  be performed?}

  \item{evalMetric}{which evaluation metric should be used.
  Supported: ARI}

  \item{expectedLabels}{supervision labels to be used if
  evalMetric is ARI}

  \item{verbose}{logical: should status information be
  printed to stdout.}

  \item{printAggSplit}{logical: should information about
  aggregation and splitting be printed to stdout.}

  \item{alpha:}{multiplicative value determining if split
  should occur}

  \item{beta:}{minimum entropy required for considering
  attribute for splitting}

  \item{lambda:}{multiplicative value determining if
  agreggation should occur}

  \item{measures:}{the measures to use, a combination of
  AMI, ARIMA, DFA, FOU, HURST, RQA}
}
\value{
  clustering result
}
\description{
  Runs the online Time Series Clustering algorithm using
  various time series analysis measures.
}

