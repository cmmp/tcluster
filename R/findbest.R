findBestAttribute = function(X, beta) {
  # first search for the attribute which gives the highest gain
  maxgain = .Machine$double.xmin
  bestAtt = -1
  outBest = NULL
  m = nrow(X) # number of series
  n = ncol(X) # number of attributes

  for(i in 1:n) {
    #if(length(unique(X[,i]))/m < beta)
    #  next
    if(myentropy(X[,i], nbins = 10) < beta)
      next
    gain = std(X[,i])
    out = weightedsd(X[,i])
    gain = gain - out$weighted_sd
    if(gain > maxgain) {
      maxgain = gain
      bestAtt = i
      outBest = out
    }
  }
  return(list(idx = bestAtt, weighted_sd = outBest$weighted_sd,
              split_point = outBest$split_point))
}
